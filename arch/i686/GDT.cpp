#include <sys/sys.h>

// define the GDT entry structure
struct gdt_entry
{
    unsigned short limit_low;
    unsigned short base_low;
    unsigned char base_middle;
    unsigned char access;
    unsigned char granularity;
    unsigned char base_high;
} __attribute__((packed));

// define the GDT pointer structure.
struct gdt_ptr
{
    unsigned short limit;
    unsigned int base;
} __attribute__((packed));

// allocate memory for the gdt and create a pointer to it.
struct gdt_entry gdt[3];
struct gdt_ptr gp;

// declare that the GDT flush is in a seperate file.
extern "C" void gdt_flush();

/**
 * @brief set and entry in the gdt.
 * 
 * @param num 
 * @param base 
 * @param limit 
 * @param access 
 * @param gran 
 */
void gdt_set_gate(int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran) {
    gdt[num].base_low = (base & 0xFFFF);
    gdt[num].base_middle = ((base >> 16) & 0xFF);
    gdt[num].base_high = ((base >> 24) & 0xFF);

    gdt[num].limit_low = (limit & 0xFFFF);
    gdt[num].granularity = (gran & 0xF0) | ((limit >> 16) & 0x0F);
    gdt[num].access = access;
}

/**
 * @brief Installs a basic GDT
 * 
 */
void gdt_install() {
    gp.limit = (sizeof(struct gdt_entry) * 3)-1;
    gp.base =(unsigned int) &gdt;

    gdt_set_gate(0,0,0,0,0);    
    gdt_set_gate(1,0,0xFFFFFFFF,0x9A,0xCF);
    gdt_set_gate(2,0,0xFFFFFFFF,0x92,0xCF);

    gdt_flush();
}