#include <sys/sys.h>
#include <libc/string.h>

// define the IDT entry structure.
struct idt_entry {
    unsigned short base_low;
    unsigned short sel;
    unsigned char always0;
    unsigned char flags;
    unsigned short base_high;
} __attribute__((packed));

// define the IDT pointer structure
struct idt_ptr {
    unsigned short limit;
    unsigned int base;
} __attribute__((packed));

// Initialize the IDT structures
struct idt_entry idt[256];
struct idt_ptr idtp;

// external function call to load the IDT
extern "C" void idt_load();

/**
 * @brief Set an IDT gate
 * 
 * @param num 
 * @param base 
 * @param sel 
 * @param flags 
 */
void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags) {
    idt[num].base_low = base & 0xFFFF;
    idt[num].sel = sel;
    idt[num].flags = flags;
    idt[num].base_high = (base >> 16) & 0xFFFF;
}

/**
 * @brief install a basic IDT
 * 
 */
void idt_install() {
    idtp.limit = (sizeof(struct idt_entry) * 256) - 1;
    idtp.base = (unsigned int)&idt;

    memset(&idt, 0, sizeof(struct idt_entry) * 256);

    isrs_install();

    idt_load();
}