#include <sys/sys.h>
#include <sys/multiboot2.h>
#include <stdint.h>

unsigned int page_directory[1024] __attribute__((aligned(4096)));
unsigned int page_table_init[1024] __attribute__((aligned(4096)));
unsigned int second_page_table[1024] __attribute__((aligned(4096)));

extern "C" void loadPageDirectory(unsigned int *);
extern "C" void enablePaging();

void paging_setup() {
    fill_init_paging();
    loadPageDirectory(page_directory);
    enablePaging();
}

void fill_init_paging() {
    for(int i = 0; i < 1024; i++) {
        page_directory[i] = (unsigned int)0x2;
    }
    for(int i = 0; i < 1024; i++) {
        page_table_init[i] = (i * 0x1000) | 3;
        second_page_table[i] = ((i + 1024) * 0x1000) | 3;
    }
    page_directory[0] = (unsigned int)page_table_init | 3;
    page_directory[1] = (unsigned int)second_page_table | 3;
}