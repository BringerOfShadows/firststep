// include a few standard type headers.
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
// include kernel headers
#include <libc/string.h>
#include <kernel/tty.h>
#include <sys/sys.h>
// include the vga definitions header.
#include "vga.h"

// define the VGA terminal constants
static const size_t VGA_HEIGHT = 25;
static const size_t VGA_WIDTH = 80;
static const uint16_t* VGA_MEMORY = (uint16_t*) 0xB8000;

// declare the global terminal variables.
static size_t terminal_row;
static size_t terminal_column;
static uint8_t terminal_color;
static uint16_t* terminal_buffer;

// declare internal functions.
void terminal_scroll();

// initialize the terminal.
void terminal_initialize() {
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = vga_entry_color(VGA_COLOR_GREEN, VGA_COLOR_BLACK);
    terminal_buffer = (uint16_t*)VGA_MEMORY;
    for(size_t y = 0; y < VGA_HEIGHT; y++) {
        for(size_t x = 0; x < VGA_WIDTH; x++) {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[index] = vga_entry(' ', terminal_color);
        }
    }
 }

// set the terminal color
void terminal_setcolor(uint8_t color) {
    terminal_color = color;
}

// write a value to the terminal VGA buffer.
void terminal_putentryat(unsigned char ch, uint8_t color, size_t x, size_t y) {
    const size_t index = y * VGA_WIDTH + x;
    if(ch != '\n') {
        terminal_buffer[index] = vga_entry(ch, color);
        if(++terminal_column == VGA_WIDTH) {
            terminal_column = 0;
            if(++terminal_row == VGA_HEIGHT) {
                // TODO: add scrolling function.
                terminal_scroll();
                //terminal_row = 0;
            }
        }
    } else {
        terminal_column = 0;
        if(++terminal_row == VGA_HEIGHT) {
            terminal_row = 0;
        }
    }
    outportb(0x3D4, 14);
    outportb(0x3D5, (terminal_row * 80 + terminal_column) >> 8);
    outportb(0x3D4, 15);
    outportb(0x3D5, (terminal_row * 80 + terminal_column));
}

void terminal_scroll() {
    size_t prevLine, line;
    size_t v, w;
    for(v=0;v<VGA_HEIGHT-2;v++) {
        for(w=0;w<VGA_WIDTH;w++) {
            prevLine = v * VGA_WIDTH + w;
            line = (v+1) * VGA_WIDTH + w;
            terminal_buffer[prevLine] = terminal_buffer[line];
        }
    }
}

// print a character to the terminal
void terminal_putchar(char ch) {
    unsigned char uc = ch;
    terminal_putentryat(uc, terminal_color, terminal_column, terminal_row);
}

// write a string of known length to the terminal
void terminal_write(const char* str, size_t len) {
    for(size_t i = 0; i < len; i++) {
        terminal_putchar(str[i]);
    }
}

// write a string of unknown length to the terminal.
void terminal_writestring(const char* str) {
    terminal_write(str, strlen(str));
}