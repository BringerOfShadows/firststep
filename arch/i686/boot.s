# Define the contnts of the Multiboot Header
.set MAGIC,		0xE85250D6              # Multiboot 2 magic value
.set ARCH,		0
.set LENGTH,	(multiboot_header_end - multiboot_header)
.set CHECKSUM,	-(MAGIC + ARCH + LENGTH)

# Store the contents of the Multiboot header at the beginning of the file.
.section .multiboot
.align 8								# Align the section on the 64-bit boundary.
multiboot_header:
.long MAGIC
.long ARCH
.long LENGTH
.long CHECKSUM
.align 8
multiboot_info_req:
.short 1        # header tag type
.short 0        # flags
.long (multiboot_info_req_end - multiboot_info_req)
.long 4         # basic memory info tag request
.long 6         # memory map info tag request
.align 8
multiboot_info_req_end:
.short 0
.short 0
.long 8
multiboot_header_end:


.section .bss
stack_bottom:
# reserve 16KB for the stack
.skip 16384
stack_top:

.section .text

.extern _main

.global _start
_start:
    # move the location the the top of the stack to the esp register.
    mov $stack_top, %esp
    # Push the multiboot stucture
    push %ebx
    # call the global constructor initialization function
    call _init
    # call the kernel main() function.
    call _main
    # call the global constructor finalization function.
    call _fini

