#include <libc/stdio.h>
#include <libc/string.h>

/**
 * @brief print a basic string
 * print a basic c-string to the screen.
 *
 * @param string 
 * @return int 
 */
int puts(const char* string) {
    size_t len = strlen(string);        // get the length of the character string
    for(size_t i = 0; i < len; i++) {   // until i equals len
        putchar(string[i]);             // print each character of the string
    }
    return 0;
}