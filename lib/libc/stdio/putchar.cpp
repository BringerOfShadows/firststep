#include <libc/stdio.h>

// include the kernel terminal header if compiling for the kernel.
#if defined (__is_libk)
#include <kernel/tty.h>
#endif

/**
 * @brief print a character to the screen
 *
 * @param ic 
 * @return int 
 */
int putchar(int ic) {
    // use terminal print call if compiling for the kernel.
    #if defined (__STDC_HOSTED__)
    // TODO: implement user console print call
    #else
    // TODO: implement kernel terminal print call
    terminal_putchar((char) ic);        // pass the character to the terminal print function.
    #endif
    return ic;
}