#include <libc/stdio.h>
#include <libc/string.h>

// Character array for integer to hexadecimal string conversion
const char HEX_TABLE[] = {
    '0', '1', '2', '3',
    '4', '5', '6', '7',
    '8', '9', 'A', 'B',
    'C', 'D', 'E', 'F'
};

// Character array for integer to decimal string conversion
const char DEC_TABLE[] = {
    '0', '1', '2', '3', '4',
    '5', '6', '7', '8', '9'
};

/**
 * @brief Prints a character variable in hexadecimal
 * 
 * @param ch 
 */
void printHexChar(unsigned char ch) {
    char out[2];                            // Container for Hexadecimal string conversion of ch
    puts("0x");                             // Print out "0x" to show that its a hexidecimal value
    out[0] = HEX_TABLE[(ch >> 4)];          // convert upper 4 bits of ch to hexadecimal character.
    out[1] = HEX_TABLE[(ch & 0xF)];         // convert lower 4 bits of ch to hexadecimal character.
    puts(out);                              // print the hexidecimal string.
}

/**
 * @brief Prints a short variable in hexadecimal.
 * 
 * @param sh 
 */
void printHexShort(unsigned short sh) {
    char out[4];
    puts("0x");
    out[0] = HEX_TABLE[(sh >> 12)];
    out[1] = HEX_TABLE[(sh >> 8) & 0xF];
    out[2] = HEX_TABLE[(sh >> 4) & 0xF];
    out[3] = HEX_TABLE[(sh & 0xF)];
    puts(out);
}

/**
 * @brief Prints an integer variable in hexadecimal
 * 
 * @param in 
 */
void printHexInt(unsigned int in) {
    char out[8];
    puts("0x");
    out[0] = HEX_TABLE[(in >> 28)];
    out[1] = HEX_TABLE[(in >> 24) & 0xF];
    out[2] = HEX_TABLE[(in >> 20) & 0xF];
    out[3] = HEX_TABLE[(in >> 16) & 0xF];
    out[4] = HEX_TABLE[(in >> 12) & 0xF];
    out[5] = HEX_TABLE[(in >> 8) & 0xF];
    out[6] = HEX_TABLE[(in >> 4) & 0xF];
    out[7] = HEX_TABLE[(in & 0xF)];
    puts(out);
}

/**
 * @brief Prints an unsigned decimal integer to screen.
 * 
 * @param in 
 */
void printUnsignedDecimal(unsigned int in) {
    unsigned int temp = in;     // create a temporary container for the value of in
    int len = 0;                // create a length variable set to 0
    if(temp == 0) {             // if the value of temp(in) is equal to 0,
        puts("0");              // print "0" to screen.
        return;                 // return from function.
    } else {                    // otherwise
        do {
            len++;              // increment len by 1
            temp /= 10;         // divide temp by ten
        } while(temp != 0);     // repeat until temp equals 0
        char revBuf[len];       // create a character array of length len.
        for(int i = 0; i < len; i++) {          // until i = len
            revBuf[i] = DEC_TABLE[in % 10];     // set the i'th position of revBuf to the character
            // conversion of the remainder of in divided by 10
            in /= 10;                       // divide in by 10
        }
        for(int i = 0; i < len; i++) {          // until i equals len
            putchar(revBuf[len - (1+i)]);       // print each character of revBuf in reverse order.
        }
    }
}

/**
 * @brief print a signed decimal value
 * 
 * @param in 
 */
void printSignedDecimal(int in) {
    int temp = in;                      // create a temporary container for in
    int len = 0;                        // create an integer variable len set to 0
    if(in == 0) {                       // if in is equal to 0
        puts("0");                      // print "0" to screen
        return;
    }
    if(in < 0) {                        // if in is negative
        putchar('-');                   // print a negative sign
        temp = -temp;                   // negate the negative in temp
    }
    // determin the number of digits in temp
    do {
        len++;
        temp /= 10;
    } while(temp != 0);
    char revBuf[len];           // set revBuf array to len length
    // negate the negative in 'in' if 'in' is negative.
    if(in < 0) {
        in = -in;
    }
    // fill revBuf with the character conversions of 'in'
    for(int i = 0; i < len; i++) {
        revBuf[i] = DEC_TABLE[in % 10];
        in /= 10;
    }
    // print revBuf in reverse order.
    for(int i = 0; i < len; i++) {
        putchar(revBuf[len - (1+i)]);
    }
}

// TODO: include formatting tag options for printf().
/**
 * @brief print a C-formatted string
 * 
 * @param format
 * @param ... 
 * @return int 
 */
int printf(const char* __restrict__ format, ...) {
    const char* str = (const char *)format;
    va_list vlist;
    va_start(vlist, format);
    int i=0;
    while(str[i]) {
        if(str[i] == '%') {
            i++;
            switch(str[i]) {
                case 'c':
                case 'C':
                    putchar(va_arg(vlist, int));
                    i++;
                    break;
                case 'x':
                case 'X':
                    if(48 <= str[i+1] && str[i+1] <= 57) {
                        i++;
                        switch(str[i]) {
                            case '2':
                                printHexChar(va_arg(vlist, int));
                                i++;
                                break;
                            case '4':
                                printHexShort(va_arg(vlist, int));
                                i++;
                                break;
                            case '8':
                                printHexInt(va_arg(vlist, int));
                                i++;
                                break;
                        }
                    } else {
                        printHexInt(va_arg(vlist, int));
                        i++;
                    }
                    break;
                case '%':
                    putchar('%');
                    i++;
                    break;
                case 'u':
                case 'U':
                    printUnsignedDecimal(va_arg(vlist, unsigned int));
                    i++;
                    break;
                case 'd':
                case 'D':
                    printSignedDecimal(va_arg(vlist, int));
                    i++;
                    break;
                default:
                    puts("\nError: invalid operator\n");
                    i++;
                    return 1;
            }
        } else {
            putchar(str[i]);
            i++;
        }
    }
    va_end(vlist);
    return 0;
}

