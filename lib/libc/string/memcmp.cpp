#include <libc/string.h>

/**
 * @brief compare the contents of two memory locations.
 * if the value of aptr is less than bptr, the function returns -1.
 * if the value of aptr is greater than btpr, return 1.
 * if the values are equal for all of size, return 0.
 * 
 * @param aptr 
 * @param bptr 
 * @param size 
 * @return int 
 */
int memcmp(const void* aptr, const void* bptr, size_t size) {
    const unsigned char* a = (const unsigned char*)aptr;
    const unsigned char* b = (const unsigned char*)bptr;
    for(size_t i = 0; i < size; i++) {
        if(a[i] < b[i]) {
            return -1;
        }
        else if(a[i] > b[i]) {
            return 1;
        }
    }
    return 0;
}