
#include <libc/string.h>

/**
 * @brief determine the length of a c-string.
 * 
 * @param str 
 * @return size_t 
 */
size_t strlen(const char* str) {
	int len = 0;
	while(str[len]) {
		len++;
	}
	return len;
}