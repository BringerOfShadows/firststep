#include <libc/string.h>

/**
 * @brief set the values of a range of memory
 * sets a section of memory to value for len bytes.
 * 
 * @param dest 
 * @param val 
 * @param len 
 * @return void* 
 */
void* memset(void* dest, int val, size_t len) {
    unsigned char* buf = (unsigned char*)dest;
    for(size_t i = 0; i < len; i++) {
        buf[i] = (unsigned char)val;
    }
    return dest;
}