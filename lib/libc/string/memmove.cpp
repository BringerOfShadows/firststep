#include <libc/string.h>

/**
 * @brief move memory from one location to another.
 * move the memory from srcptr to dstptr, then return
 * the value of dstprt.
 * 
 * @param dstptr 
 * @param srcptr 
 * @param len 
 * @return void* 
 */
void* memmove(void* dstptr, const void* srcptr, size_t len) {
    unsigned char* dst = (unsigned char*)dstptr;
    const unsigned char* src = (const unsigned char*)srcptr;
    if(dst < src) {
        for(size_t i = 0; i < len; i++) {
            dst[i] = src[i];
        }
    } else {
        for(size_t i = len; i != 0; i--) {
            dst[i-1] = src[i-1];
        }
    }

    return dstptr;
}