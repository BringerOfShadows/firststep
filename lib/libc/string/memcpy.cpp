#include <libc/string.h>

/**
 * @brief copy a section of memory to another.
 * copies the values referenced from srcptr to dstptr, 
 * then returns the value of dstprt.
 * 
 * @param dstprt 
 * @param srcptr 
 * @param len 
 * @return void* 
 */
void* memcpy(void* __restrict dstptr, const void* __restrict srcptr, size_t len) {
    unsigned char* dest = (unsigned char*)dstptr;
    const unsigned char* src = (const unsigned char*)srcptr;
    for(size_t i = 0; i < len; i++) {
        dest[i] = src[i];
    }
    return dstptr;
}