#ifndef _STRING_H
#define _STRING_H 1

// include the standard definitions header.
#include <stddef.h>

// use C-linkage if compiling in C++
#ifdef __cplusplus
extern "C" {
#endif

// libc string manipulation functions.
int memcmp(const void*, const void*, size_t);
void* memcpy(void* __restrict, const void* __restrict, size_t);
void* memmove(void*, const void*, size_t);
void* memset(void*, int, size_t);
size_t strlen(const char*);

#ifdef __cplusplus
}
#endif

#endif