#ifndef _STDIO_H
#define _STDIO_H 1

// define the End-of-File value.
#define EOF (-1)

// include the standard arguments header
#include <stdarg.h>

//use C-linkage if compiling as C++
#ifdef __cplusplus
extern "C" {
#endif

int printf(const char* __restrict, ...);    // print a C-formatted string
int putchar(int);                           // print a character
int puts(const char*);                      // print a basic string

#ifdef __cplusplus
}
#endif

#endif