#ifndef _TTY_H
#define _TTY_H 1

// inlcude the standard definitions header.
#include <stddef.h>

// kernel terminal functions.
void terminal_initialize();
void terminal_putchar(char);
void terminal_write(const char*, size_t);
void terminal_writestring(const char*);

#endif