#ifndef _SYS_H
#define _SYS_H 1

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

struct regs {
    unsigned int gs, fs, es, ds; 
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
    unsigned int int_no, err_code;
    unsigned int eip, cs, eflags, usersp, ss;
};

// port I/O functions in sys.cpp
void outportb(unsigned short, unsigned char);
unsigned char inportb(unsigned short);

// gdt functions in GDT.cpp
void gdt_set_gate(int, unsigned long, unsigned long, unsigned char, unsigned char);
void gdt_install();

// idt functions in IDT.cpp
void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags);
void idt_install();

// isr functions in isrs.cpp
void isrs_install();
void fault_handler(struct regs*);

/*
// serial i/o functions in serial.cpp
#define PORT 0x3f8

void init_serial();
int serial_recieved();
char read_serial();
int is_transmit_empty();
void write_serial(char a);
*/

// paging functions in paging.cpp
void paging_setup();
void fill_init_paging();

#if defined(__cplusplus)
}
#endif

#endif