#include <sys/sys.h>
#include <sys/multiboot2.h>
#include <kernel/tty.h>
#include <libc/stdio.h>

#include <stdint.h>

struct multiboot2_data {
    multiboot_uint32_t totalSize;
    multiboot_uint32_t reserved;
    struct multiboot_tag_basic_meminfo memInfo;
    struct multiboot_tag_mmap mmapInfo;
}__attribute__((packed));

extern "C" void _main(unsigned long mbd) {
    struct multiboot_tag *tag;
    struct multiboot2_data hold;
    gdt_install();
    idt_install();
    paging_setup();
    terminal_initialize();
    terminal_writestring("Hello World! \n");
    printf("MBI location: %x\n", mbd);
    unsigned size = *(unsigned *) mbd;
    printf("MBI size: %x\n", size);
    for(tag = (struct multiboot_tag*) (mbd + 8);
        tag->type != MULTIBOOT_TAG_TYPE_END;
        tag = (struct multiboot_tag *) ((multiboot_uint8_t *) tag + ((tag->size + 7) & ~7))) {
        
        switch (tag->type) {
            case MULTIBOOT_TAG_TYPE_MMAP:
                printf("Tag %x, Size %x4\n", tag->type, tag->size);
                hold.mmapInfo = *((multiboot_tag_mmap *) tag);
                printf("Memory Map size: %x\n\n", hold.mmapInfo.size);
                multiboot_memory_map_t* mmap;
                
                printf("mmap:\n");
                for(mmap = ((struct multiboot_tag_mmap *) tag)->entries;
                    (multiboot_uint8_t *) mmap < (multiboot_uint8_t *) tag + tag->size;
                    mmap = (multiboot_memory_map_t *) ((unsigned long) mmap + 
                    ((struct multiboot_tag_mmap *) tag)->entry_size)) {
                        printf("base address: %x length: %x type: %x2 \n", multiboot_uint32_t(mmap->addr & 0xFFFFFFFF),
                        multiboot_uint32_t(mmap->len & 0xFFFFFFFF), mmap->type);
                        //printf("length: %x \t")
                    }
                break;
            case MULTIBOOT_TAG_TYPE_BASIC_MEMINFO:
                printf("Tag %x, Size %x4\n", tag->type, tag->size);
                hold.memInfo = *(multiboot_tag_basic_meminfo *) tag;
                printf("Lower Memory: %dKB\n", hold.memInfo.mem_lower);
                printf("Upper Memory: %dKB\n", hold.memInfo.mem_upper);
                break;
            default:
                break;
        }
    }
    printf("Version: 0.0.0.1G\n");
    
    for(;;);
}

