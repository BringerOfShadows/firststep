#include <sys/sys.h>

/**
 * @brief send data to a port.
 * write and 8-bit value to a port
 * 
 * @param _port 
 * @param _data 
 */
void outportb(unsigned short _port, unsigned char _data) {
    __asm__ __volatile__("outb %1, %0" : : "dN" (_port), "a" (_data));
}

/**
 * @brief retrieve data from a port
 * read an 8-bit value from a port
 * 
 * @param _port 
 * @return unsigned char 
 */
unsigned char inportb(unsigned short _port) {
    unsigned char rv;
    __asm__ __volatile__("inb %1, %0" : "=a" (rv) : "dN" (_port));
    return rv;
}