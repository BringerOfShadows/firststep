
function set_archs {
    ARCHDIR=$1
    TARGET=$1-elf
}

if [ -z $1 ]; then
    set_archs i686
else
    set_archs $1
fi
rm -f Makefile
echo "export ARCHDIR:=$ARCHDIR" > Makefile
echo "export TARGET:=$TARGET" >> Makefile
echo "export CC:=$TARGET-gcc" >> Makefile
echo "export CXX:=$TARGET-g++" >> Makefile
echo "export AS:=$TARGET-as" >> Makefile
echo "export LD:=$TARGET-ld" >> Makefile
echo "export AR:=$TARGET-ar" >> Makefile
echo "export OBJC:=$TARGET-objcopy" >> Makefile
echo "" >> Makefile
cat mkfile.temp >> Makefile