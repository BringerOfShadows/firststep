export ARCHDIR:=i686
export TARGET:=i686-elf
export CC:=i686-elf-gcc
export CXX:=i686-elf-g++
export AS:=i686-elf-as
export LD:=i686-elf-ld
export AR:=i686-elf-ar
export OBJC:=i686-elf-objcopy

export CFLAGS:=-O2 -g -ffreestanding -nostdlib -Wall -Wextra
export CPPFLAGS:=-I$(CURDIR)/include -fno-exceptions -fno-rtti
export LIBS:=libc

CRTBEGIN_OBJ:=$(shell $(CPP) $(CFLAGS) $(CPPFLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ:=$(shell $(CPP) $(CFLAGS) $(CPPFLAGS) -print-file-name=crtend.o)

LINK_LIST := arch/crti.o $(CRTBEGIN_OBJ) arch/arch.o kernel/kern.o lib/libk.a -lgcc $(CRTEND_OBJ) arch/crtn.o
testOS.elf:$(LIBS) arch.o kern.o
	$(CXX) $(CFLAGS) $(CPPFLAGS) -T arch/$(ARCHDIR)/linker.ld -o $@ $(LINK_LIST)
	$(OBJC) --only-keep-debug $@ symbols.sym
	$(OBJC) --strip-debug $@

$(LIBS):
	echo $(CPP)
	$(MAKE) --directory=lib/$@ CPPFLAGS="$(CPPFLAGS) -D __is_libk"

arch.o:
	$(MAKE) --directory=arch/$(ARCHDIR)

kern.o:
	$(MAKE) --directory=kernel

clean:
	$(MAKE) --directory=lib/libc clean
	$(MAKE) --directory=arch/$(ARCHDIR) clean
	$(MAKE) --directory=kernel clean
	rm -f arch/*.o
	rm -f kernel/*.o
	rm -f *.elf *.sym